import {
  Auth,
  createUserWithEmailAndPassword,
  getAuth,
  onAuthStateChanged,
  signInWithEmailAndPassword,
  updateProfile,
} from "firebase/auth";
import {
  DataSnapshot,
  Database,
  Unsubscribe,
  get,
  getDatabase,
  onChildAdded,
  onChildChanged,
  onChildRemoved,
  onDisconnect,
  onValue,
  ref,
  remove,
  set,
} from "firebase/database";
import { TurnData } from "../classes/battle/player-battle";
import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyDu00DYkDONybkg8cfHrOVNqW0wO9GwImI",
  authDomain: "adventure-page-9549e.firebaseapp.com",
  databaseURL: "https://adventure-page-9549e-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "adventure-page-9549e",
  storageBucket: "adventure-page-9549e.appspot.com",
  messagingSenderId: "978417258796",
  appId: "1:978417258796:web:edd1c77bcce855c2e85c89",
};

export class FirebaseService {
  private static _instance: FirebaseService;
  private readonly _currentLogin = Date.now();
  private _currentSessionEmail: string;
  private _db: Database;
  private _auth: Auth;

  private constructor() {
    this._db = getDatabase(initializeApp(firebaseConfig));
    this._auth = getAuth();
  }

  static get instance(): FirebaseService {
    if (!this._instance) {
      this._instance = new FirebaseService();
    }
    return this._instance;
  }

  get currentUserName() {
    return this._auth.currentUser.displayName;
  }

  addOnChildAddedListener(path: string, callback: (snapshot: DataSnapshot) => void): Unsubscribe {
    return onChildAdded(ref(this._db, path), callback);
  }

  addOnChildChangedListener(path: string, callback: (snapshot: DataSnapshot) => void): Unsubscribe {
    return onChildChanged(ref(this._db, path), callback);
  }

  addOnChildRemovedListener(path: string, callback: (snapshot: DataSnapshot) => void): Unsubscribe {
    return onChildRemoved(ref(this._db, path), callback);
  }

  addOnValueChangedListener(path: string, callback: (snapshot: DataSnapshot) => void): Unsubscribe {
    return onValue(ref(this._db, path), callback);
  }

  // Authorization
  async signUp(email: string, password: string, confirmPassword: string, username: string): Promise<void> {
    if (!email.length) {
      throw new Error("Email cannot be empty");
    }
    if (username.length < 3) {
      throw new Error("Username must be at least 3 characters");
    }
    if (password.length < 6) {
      throw new Error("Password must be at least 6 characters");
    }
    if (password !== confirmPassword) {
      throw new Error("Password are not matching");
    }
    return get(ref(this._db, `${Key.players}/${username}`))
      .then((snapshot) => {
        if (snapshot.val()) {
          throw new Error("Invalid username");
        }
      })
      .then(() => {
        return createUserWithEmailAndPassword(this._auth, email, password).catch(() => {
          throw new Error("Invalid email");
        });
      })
      .then(() => {
        return updateProfile(this._auth.currentUser, { displayName: username });
      })
      .then(() => {
        return this.signIn(email, password);
      });
  }

  async signIn(email: string, password: string): Promise<void> {
    return signInWithEmailAndPassword(this._auth, email, password)
      .then(() => {
        onDisconnect(ref(this._db, `${Key.currentlyOnline}/${this.currentUserName}`)).remove();
        onDisconnect(ref(this._db, `${Key.flaggingPlayers}/${this.currentUserName}`)).remove();
        onDisconnect(ref(this._db, `${Key.dueling}/${this.currentUserName}`)).remove();
        return set(ref(this._db, `${Key.currentlyOnline}/${this.currentUserName}`), this._currentLogin);
      })
      .then(() => {
        this._currentSessionEmail = email;
        onAuthStateChanged(this._auth, (user) => {
          if (user.email != this._currentSessionEmail) {
            window.location.reload();
          }
        });
        onValue(ref(this._db, `${Key.currentlyOnline}/${this.currentUserName}`), (snapshot) => {
          if (!snapshot.val()) {
            set(ref(this._db, `${Key.currentlyOnline}/${this.currentUserName}`), this._currentLogin);
          } else if (snapshot.val() != this._currentLogin) {
            window.location.reload();
          }
        });
      });
  }

  // chat
  sendMessage(message: string) {
    return set(ref(this._db, Key.message), message);
  }

  // map
  getScreen(boardPosX: number, boardPosY: number): Promise<DataSnapshot> {
    return get(ref(this._db, `${Key.map}/${boardPosX}/${boardPosY}`));
  }

  // PvP
  setFlagForPvP(value: boolean) {
    if (value) {
      return set(ref(this._db, `${Key.flaggingPlayers}/${this.currentUserName}`), false);
    } else {
      return remove(ref(this._db, `${Key.flaggingPlayers}/${this.currentUserName}`));
    }
  }

  async sendDuel(enemyPlayerName: string) {
    return get(ref(this._db, `${Key.flaggingPlayers}/${enemyPlayerName}`)).then((snapshot) => {
      if (snapshot.val() == undefined) throw "error";
      return set(ref(this._db, `${Key.flaggingPlayers}/${enemyPlayerName}`), this.currentUserName);
    });
  }

  setUpDuel(playerName: string, playerLevel: number, playerHealth: number) {
    return set(ref(this._db, `${Key.dueling}/${this.currentUserName}`), {
      [Key.enemyPlayer]: playerName,
      [Key.level]: playerLevel,
      [Key.maxHealth]: playerHealth,
    });
  }

  async setTurnData(turnData: TurnData) {
    return remove(ref(this._db, `${Key.dueling}/${this.currentUserName}/${Key.turnData}`)).then(() => {
      return set(ref(this._db, `${Key.dueling}/${this.currentUserName}/${Key.turnData}`), turnData);
    });
  }

  cleanUpDuel() {
    return remove(ref(this._db, `${Key.dueling}/${this.currentUserName}`));
  }

  // character
  getCurrentPlayerPositions() {
    return get(ref(this._db, `${Key.players}/${this.currentUserName}/${Key.positions}`));
  }

  getPlayerPositions(userName: string) {
    return get(ref(this._db, `${Key.players}/${userName}/${Key.positions}`));
  }

  setCharacterPosition(boardPosX: number, boardPosY: number, charPosX: number, charPosY: number): Promise<void> {
    return set(ref(this._db, `${Key.players}/${this.currentUserName}/${Key.positions}`), {
      [Key.boardPosition]: {
        [Key.boardPosX]: boardPosX,
        [Key.boardPosY]: boardPosY,
      },
      [Key.charPosition]: {
        [Key.charPosX]: charPosX,
        [Key.charPosY]: charPosY,
      },
    });
  }

  getCurrentPlayerStats() {
    return get(ref(this._db, `${Key.players}/${this.currentUserName}/${Key.stats}`));
  }

  getPlayerStats(userName: string) {
    return get(ref(this._db, `${Key.players}/${userName}/${Key.stats}`));
  }

  setCurrentPlayerStats(stats: object) {
    return set(ref(this._db, `${Key.players}/${this.currentUserName}/${Key.stats}`), stats);
  }
}

export enum Key {
  map = "map",
  message = "message",
  players = "players",
  stats = "stats",
  isOnline = "isOnline",
  currentLogin = "currentLogin",
  currentlyOnline = "currentlyOnline",
  positions = "positions",
  boardPosition = "boardPosition",
  boardPosX = "boardPosX",
  boardPosY = "boardPosY",
  charPosition = "charPosition",
  charPosX = "charPosX",
  charPosY = "charPosY",
  dueling = "dueling",
  enemyPlayer = "enemyPlayer",
  flaggingPlayers = "flaggingPlayers",
  turnData = "turnData",
  level = "level",
  maxHealth = "maxHealth",
}
