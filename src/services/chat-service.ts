import { FirebaseService, Key } from "./firebase-service";
import { PlayerCharacter } from "../classes/character";

export class ChatService {
  private static _instance: ChatService;
  private _firebaseService = FirebaseService.instance;
  private _messageWindow: HTMLElement = document.getElementById("messageContainer");
  private _chatInput: HTMLInputElement = document.getElementById("chatInput") as HTMLInputElement;
  private _firstMessage = true;
  private _commandOnCooldown = false;
  private _messageCount = 0;

  private constructor() {
    this._chatInput.addEventListener("keyup", (event: KeyboardEvent) => {
      if (event.key === "Enter") {
        this.sendMessage();
        event.stopPropagation();
      }
    });
    this._firebaseService.addOnValueChangedListener(Key.message, (snapshot) => {
      if (this._firstMessage) {
        this._firstMessage = false;
        return;
      }

      if ((snapshot.val() as string).startsWith(this._firebaseService.currentUserName + ":")) return;
      this.printMessage(snapshot.val());
    });
  }

  static get instance(): ChatService {
    if (!this._instance) {
      this._instance = new ChatService();
    }
    return this._instance;
  }

  private sendMessage() {
    if (this._chatInput.value.startsWith("/")) this.chatCommand();
    else if (this._chatInput.value) {
      this._firebaseService.sendMessage(`${this._firebaseService.currentUserName}: ${this._chatInput.value}`);
      this.printMessage(`You: ${this._chatInput.value}`);
    } else {
      this._chatInput.blur();
    }
    this._chatInput.value = "";
  }

  chatCommand() {
    if (this._commandOnCooldown) {
      this.printMessage("System: Commands have a cooldown of 10 seconds", "gray");
      return;
    }
    if (PlayerCharacter.instance.currentlyBattling) {
      this.printMessage("System: Commands cannot be used during battle", "gray");
      return;
    }

    const commandAndArguments = this._chatInput.value.split(" ", 2);
    switch (commandAndArguments[0]) {
      case ChatCommands.flag: {
        PlayerCharacter.instance.toggleFlag();
        this.setCommandTimeout();
        break;
      }
      case ChatCommands.duel: {
        if (commandAndArguments.length <= 1) {
          this.printMessage("System: Specify the player you want to duel", "gray");
          break;
        }
        if (PlayerCharacter.instance.currentlyFlagging) {
          this.printMessage("System: Cannot duel whilst flagging", "gray");
          break;
        }
        const enemyPlayerName = commandAndArguments.splice(1).join(" ");
        this._firebaseService
          .sendDuel(enemyPlayerName)
          .then(() => {
            PlayerCharacter.instance.awaitDuel(enemyPlayerName);
          })
          .catch(() => {
            this.printMessage(`System: Failed to duel '${enemyPlayerName}'`, "gray");
          });
        break;
      }
    }
  }

  setCommandTimeout() {
    this._commandOnCooldown = true;
    setTimeout(() => {
      this._commandOnCooldown = false;
    }, 10000);
  }

  printMessage(text: string, color = "black") {
    this._messageCount++;
    const newMessage = document.createElement("div");
    newMessage.innerHTML = text;
    newMessage.style.color = color;
    this._messageWindow.appendChild(newMessage);
    if (this._messageCount > 50) {
      this._messageWindow.firstElementChild.remove();
    }
  }

  focusChat() {
    this._chatInput.focus();
  }
}

enum ChatCommands {
  flag = "/flag",
  duel = "/duel",
}
