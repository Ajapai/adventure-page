import { Blob, Flower, Monster, Treant } from "./monster";
import { Images } from "../helpers/image-helper";

export abstract class Terrain {
  readonly image: string;
  readonly traversable: Traversable;
  readonly encounterRate: number;
  abstract encounters: Monster[];
  constructor(image: string, traversable: Traversable, encounterRate: number) {
    this.image = image;
    this.traversable = traversable;
    this.encounterRate = encounterRate;
  }

  static create(terrainType: TerrainType): Terrain {
    switch (terrainType) {
      case TerrainType.Desert: {
        return new Desert();
      }
      case TerrainType.Forest: {
        return new Forest();
      }
      case TerrainType.Grass: {
        return new Grass();
      }
      case TerrainType.Mountain: {
        return new Mountain();
      }
      case TerrainType.Mud: {
        return new Mud();
      }
      case TerrainType.Water: {
        return new Water();
      }
      default: {
        return new Mountain();
      }
    }
  }
}

export class Desert extends Terrain {
  encounters: Monster[] = [new Blob(2), new Blob(3), new Flower(3)];
  constructor() {
    super(Images.desert.src, Traversable.Always, 10);
  }
}

export class Forest extends Terrain {
  encounters: Monster[] = [new Flower(1), new Flower(2), new Treant(1), new Treant(2)];
  constructor() {
    super(Images.forest.src, Traversable.Always, 10);
  }
}

export class Grass extends Terrain {
  encounters: Monster[] = [new Blob(1), new Flower(1)];
  constructor() {
    super(Images.grass.src, Traversable.Always, 5);
  }
}

export class Mountain extends Terrain {
  encounters: Monster[] = [new Blob(99)];
  constructor() {
    super(Images.mountain.src, Traversable.Never, 0);
  }
}

export class Mud extends Terrain {
  encounters: Monster[] = [new Blob(3), new Treant(3), new Treant(4)];
  constructor() {
    super(Images.mud.src, Traversable.SpikyBoots, 15);
  }
}

export class Water extends Terrain {
  encounters: Monster[] = [new Blob(4), new Blob(5)];
  constructor() {
    super(Images.water.src, Traversable.FloatingBoots, 15);
  }
}

export enum TerrainType {
  Desert = "Desert",
  Forest = "Forest",
  Grass = "Grass",
  Mountain = "Mountain",
  Mud = "Mud",
  Water = "Water",
}

export enum Traversable {
  Always = "Always",
  SpikyBoots = "SpikyBoots",
  FloatingBoots = "FloatingBoots",
  Never = "Never",
}
