import { Battle, Stance, StanceOutcome } from "./battle";
import { Images } from "../../helpers/image-helper";
import { Monster } from "../monster";
import { RandomHelper } from "../../helpers/random-helper";

export class MonsterBattle extends Battle {
  private static _instance: MonsterBattle;
  static get instance() {
    if (!this._instance) {
      this._instance = new MonsterBattle();
    }
    return this._instance;
  }

  // Monster fields
  private _currentMonster: Monster;
  private _monsterStance: Stance;
  private _monsterHealth: number;

  get monsterHealth() {
    return this._monsterHealth;
  }

  set monsterHealth(value: number) {
    this._monsterHealth =
      value > this._currentMonster.maxHealth ? this._currentMonster.maxHealth : value < 0 ? 0 : value;
  }

  // Actions
  flee() {
    this.closeActionBar();
    if (RandomHelper.randomRange(0, 1)) {
      this.printText("Successfully ran away!", () => {
        setTimeout(() => {
          this.endBattle(true);
        }, 1000);
      });
    } else {
      this.printText("Failed to run away..", () => {
        this._stanceOutcome = StanceOutcome.Loser;
        this._playerGoesFirst = true;
        setTimeout(() => {
          this.monsterTurn();
        }, 1000);
      });
    }
  }

  // Stance
  evaluateEnemyActions() {
    this._monsterStance = this._currentMonster.generateStance();
    setTimeout(() => {
      this._enemyElement.src = this._currentMonster.getStanceImage(this._monsterStance);
    }, 300);
    this.printText(`The ${this._currentMonster.name} chose ${this._monsterStance}!`, () => {
      this._playerGoesFirst = this.wonStanceBattle(this._monsterStance, () => {
        this._stanceOutcome = StanceOutcome.Draw;
        return !!RandomHelper.randomRange(0, 1);
      });
      setTimeout(this.startBattlePhase.bind(this), 1000);
    });
  }

  // Battle Phase
  startBattlePhase() {
    if (this._playerGoesFirst) {
      this.playerTurn();
    } else {
      this.monsterTurn();
    }
  }

  playerTurn() {
    if (!this._selectedAbility) {
      this.printText("You rest a bit.", () => {
        this.animationOnPlayer = Images.rest.src;
        const rested = this._playerCharacter.rest(this._stanceOutcome);
        this.displayDamageNumber(-rested.health, true, false, 500);
        this.displayDamageNumber(rested.mana, true, true, 500);
        setTimeout(this.afterPlayerTurn.bind(this), 1000);
      });
      return;
    }
    this.printText(`You use ${this._selectedAbility.name}.`, () => {
      const damage = this.applyStanceDamage(this._selectedAbility.damage, true);
      if (this._selectedAbility.targetsEnemy) {
        this.animationOnEnemy = this._selectedAbility.animationSrc;
        this.monsterHealth -= damage;
        this.displayDamageNumber(damage, false, false, this._selectedAbility.damageDelay);
      } else {
        this.animationOnPlayer = this._selectedAbility.animationSrc;
        this._playerCharacter.stats.currentHealth -= damage;
        this.displayDamageNumber(damage, true, false, this._selectedAbility.damageDelay);
      }
      this._playerCharacter.stats.currentMana -= this._selectedAbility.manaCost;
      setTimeout(this.afterPlayerTurn.bind(this), 1000);
    });
  }

  afterPlayerTurn() {
    if (this.monsterHealth <= 0) {
      this.monsterDies();
    } else if (this._playerGoesFirst) {
      this.monsterTurn();
    } else {
      this.newTurn();
    }
  }

  monsterTurn() {
    const monsterAbility = this._currentMonster.chooseAbility();
    this.printText(`${this._currentMonster.name} uses ${monsterAbility.name}.`, () => {
      const damage = this.applyStanceDamage(monsterAbility.damage, false);
      if (monsterAbility.targetsEnemy) {
        this.animationOnPlayer = monsterAbility.animationSrc;
        this._playerCharacter.stats.currentHealth -= damage;
        this.displayDamageNumber(damage, true, false, monsterAbility.damageDelay);
      } else {
        this.animationOnEnemy = monsterAbility.animationSrc;
        this.monsterHealth -= damage;
        this.displayDamageNumber(damage, false, false, monsterAbility.damageDelay);
      }
      setTimeout(this.afterMonsterTurn.bind(this), 1000);
    });
  }

  afterMonsterTurn() {
    if (this._playerCharacter.stats.currentHealth <= 0) {
      this.playerDies();
    } else if (this._playerGoesFirst) {
      this.newTurn();
    } else {
      this.playerTurn();
    }
  }

  newTurn() {
    this._playerCharacter.stats.saveCharacterStats();
    this._playerElement.src = Images.character.base.src;
    this._enemyElement.src = this._currentMonster.imageCollection.base.src;
    this.printText("Select an action.", this.openActionBar.bind(this));
  }

  // End Phase
  monsterDies() {
    this.deathAnimation(this._enemyElement);
    this._playerCharacter.stats.addExp(this._currentMonster.expValue);
    this.printText("Victory!", () => {
      setTimeout(() => {
        this.endBattle(true);
      }, 1000);
    });
  }

  playerDies() {
    document.getElementById("board").appendChild(this._gameOverScreen);
    this.deathAnimation(this._playerElement);
    setTimeout(() => {
      this._gameOverScreen.style.opacity = "1";
      setTimeout(() => {
        this.endBattle(false);
        this._playerCharacter.resetOnDeath();
        setTimeout(() => {
          this._gameOverScreen.style.opacity = "0";
          setTimeout(this._playerCharacter.unblockMovement.bind(this._playerCharacter), 2000);
          setTimeout(this._gameOverScreen.remove.bind(this._gameOverScreen), 4000);
        }, 2000);
      }, 3000);
    }, 1000);
  }

  // Battle Transitions
  innitiateBattle(monster: Monster) {
    this.currentlyBattling = true;
    this.newBattle();

    this._fleeButton.onclick = this.flee.bind(this);
    this._fleeButton.title = "50% chance to flee from battle.";

    this._currentMonster = monster;
    this._monsterHealth = this._currentMonster.maxHealth;
    this._playerElement.src = Images.character.base.src;
    this._enemyElement.src = this._currentMonster.imageCollection.base.src;

    this.openBattleScreen();
    this.loadPlayerSkills();
    this.printText(monster.battleText, () => {
      this.openActionBar();
    });
  }

  endBattle(unblock: boolean) {
    this.closeBattleScreen();
    this.blockButtons(this._actionButtons);
    setTimeout(() => {
      if (unblock) this._playerCharacter.unblockMovement();
      this.resetElement(this._playerElement);
      this.resetElement(this._enemyElement);
      this.currentlyBattling = false;
    }, 1000);
    this._playerCharacter.stats.saveCharacterStats();
  }
}
