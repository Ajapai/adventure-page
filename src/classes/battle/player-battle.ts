import { AbilitiyType, Ability } from "../ability";
import { Battle, Stance, StanceOutcome } from "./battle";
import { FirebaseService, Key } from "../../services/firebase-service";
import { Images } from "../../helpers/image-helper";
import { RandomHelper } from "../../helpers/random-helper";
import { Unsubscribe } from "firebase/database";

export class PlayerBattle extends Battle {
  private _unsubscribe: Unsubscribe;

  private _fleeCounter: number;
  private _playerHealthPriorToBattle: number;
  private _playerManaPriorToBattle: number;
  private _enemyLevel: number;
  private _enemyHealth: number;
  private _enemyMaxHealth: number;
  private _enemyPlayerName: string;
  private _playerIsCoinFlipper: boolean;
  private _enemyTurnData: TurnData;
  private _receivedEnemyData = false;
  private _playerIsCoinFlipWinner: boolean;
  private _selectedAbilityDamage: number;

  private static _instance: PlayerBattle;
  static get instance() {
    if (!this._instance) {
      this._instance = new PlayerBattle();
    }
    return this._instance;
  }

  get enemyHealth() {
    return this._enemyHealth;
  }

  set enemyHealth(value: number) {
    this._enemyHealth = value > this._enemyMaxHealth ? this._enemyMaxHealth : value < 0 ? 0 : value;
  }

  // Actions
  flee(): void {
    this.closeActionBar();
    switch (this._fleeCounter++) {
      case 0: {
        this.printText("You cannot flee from another Player", this.openActionBar.bind(this));
        break;
      }
      case 1: {
        this.printText("You will give up if you try to flee again", this.openActionBar.bind(this));
        break;
      }
      case 2: {
        this.printText("You gave up", () => {
          this.endBattle(true);
        });
        break;
      }
    }
  }

  // Sync between players
  evaluateEnemyActions() {
    this._playerIsCoinFlipWinner = !!RandomHelper.randomRange(0, 1);
    this._selectedAbilityDamage = this._selectedAbility?.damage ?? 0;
    FirebaseService.instance.setTurnData({
      selectedAbility: this._selectedAbility?.type ?? AbilitiyType.Empty,
      abilityDamage: this._selectedAbilityDamage,
      selectedStance: this._selectedStance,
      coinFlipWinner: this._playerIsCoinFlipper ? this._playerIsCoinFlipWinner : null,
    });

    if (this._receivedEnemyData) {
      this.processEnemyData();
    } else {
      this.printText(`Wait for ${this._enemyPlayerName}...`, () => {
        const awaitEnemyData = () => {
          if (this._receivedEnemyData) {
            this.processEnemyData();
          } else {
            setTimeout(awaitEnemyData.bind(this), 100);
          }
        };
        awaitEnemyData();
      });
    }
  }

  processEnemyData() {
    this._receivedEnemyData = false;
    setTimeout(() => {
      this.applyEnemyStanceImage(this._enemyTurnData.selectedStance);
    }, 300);
    this.printText(`The ${this._enemyPlayerName} chose ${this._enemyTurnData.selectedStance}!`, () => {
      this._playerGoesFirst = this.wonStanceBattle(this._enemyTurnData.selectedStance, () => {
        return this._playerIsCoinFlipper ? this._playerIsCoinFlipWinner : !this._enemyTurnData.coinFlipWinner;
      });
      setTimeout(this.startBattlePhase.bind(this), 1000);
    });
  }

  applyEnemyStanceImage(stance: Stance) {
    switch (stance) {
      case Stance.Speed: {
        this._enemyElement.src = Images.character.speed.src;
        break;
      }
      case Stance.Power: {
        this._enemyElement.src = Images.character.power.src;
        break;
      }
      case Stance.Technique: {
        this._enemyElement.src = Images.character.technique.src;
        break;
      }
    }
  }

  // Battle Phase
  startBattlePhase() {
    if (this._playerGoesFirst) {
      this.playerTurn();
    } else {
      this.enemyPlayerTurn();
    }
  }

  playerTurn() {
    if (!this._selectedAbility) {
      this.printText("You rest a bit.", () => {
        this.animationOnPlayer = Images.rest.src;
        const rested = this._playerCharacter.rest(this._stanceOutcome);
        this.displayDamageNumber(-rested.health, true, false, 500);
        this.displayDamageNumber(rested.mana, true, true, 500);
        setTimeout(this.afterPlayerTurn.bind(this), 1000);
      });
      return;
    }
    this.printText(`You use ${this._selectedAbility.name}.`, () => {
      const damage = this.applyStanceDamage(this._selectedAbilityDamage, true);
      if (this._selectedAbility.targetsEnemy) {
        this.animationOnEnemy = this._selectedAbility.animationSrc;
        this.enemyHealth -= damage;
        this.displayDamageNumber(damage, false, false, this._selectedAbility.damageDelay);
      } else {
        this.animationOnPlayer = this._selectedAbility.animationSrc;
        this._playerCharacter.stats.currentHealth -= damage;
        this.displayDamageNumber(damage, true, false, this._selectedAbility.damageDelay);
      }
      this._playerCharacter.stats.currentMana -= this._selectedAbility.manaCost;
      setTimeout(this.afterPlayerTurn.bind(this), 1000);
    });
  }

  afterPlayerTurn() {
    if (this._enemyHealth <= 0) {
      this.enemyLooses();
    } else if (this._playerGoesFirst) {
      this.enemyPlayerTurn();
    } else {
      this.newTurn();
    }
  }

  enemyPlayerTurn() {
    if (this._enemyTurnData.selectedAbility == AbilitiyType.Empty) {
      this.printText(`${this._enemyPlayerName} revovers a bit.`, () => {
        this.animationOnEnemy = Images.rest.src;
        this.enemyHealth -= Math.round(
          (this._enemyMaxHealth / 20) *
            (this._stanceOutcome == StanceOutcome.Winner ? 0.5 : this._stanceOutcome == StanceOutcome.Draw ? 1 : 0.5),
        );
        setTimeout(this.afterEnemyPlayerTurn.bind(this), 1000);
      });
      return;
    }
    const enemyPlayerAbility = Ability.create(this._enemyTurnData.selectedAbility, this._enemyLevel);
    this.printText(`${this._enemyPlayerName} uses ${enemyPlayerAbility.name}.`, () => {
      const damage = this.applyStanceDamage(this._enemyTurnData.abilityDamage, false);
      if (enemyPlayerAbility.targetsEnemy) {
        this.animationOnPlayer = enemyPlayerAbility.animationSrc;
        this._playerCharacter.stats.currentHealth -= damage;
        this.displayDamageNumber(damage, true, false, enemyPlayerAbility.damageDelay);
      } else {
        this.animationOnEnemy = enemyPlayerAbility.animationSrc;
        this.enemyHealth -= damage;
        this.displayDamageNumber(damage, false, false, enemyPlayerAbility.damageDelay);
      }
      setTimeout(this.afterEnemyPlayerTurn.bind(this), 1000);
    });
  }

  afterEnemyPlayerTurn() {
    if (this._playerCharacter.stats.currentHealth <= 0) {
      this.playerLooses();
    } else if (this._playerGoesFirst) {
      this.newTurn();
    } else {
      this.playerTurn();
    }
  }

  newTurn() {
    this._playerElement.src = Images.character.base.src;
    this._enemyElement.src = Images.character.base.src;
    this.printText("Select an action.", this.openActionBar.bind(this));
  }

  // End Phase
  enemyLooses() {
    this.deathAnimation(this._enemyElement);
    this.printText("Victory!", () => {
      setTimeout(() => {
        this.endBattle(true);
      }, 2000);
    });
  }
  playerLooses() {
    this.deathAnimation(this._playerElement);
    this.printText("Defeat...", () => {
      setTimeout(() => {
        this.endBattle(true);
      }, 2000);
    });
  }

  // Battle Transitions
  innitiateBattle(
    enemyPlayerName: string,
    enemyPlayerLevel: number,
    enemyPlayerMaxHealth: number,
    playerIsCoinFlipper: boolean,
  ) {
    this.currentlyBattling = true;
    this.newBattle();

    // Flee function
    this._fleeCounter = 0;
    this._fleeButton.onclick = this.flee.bind(this);
    this._fleeButton.title = "Press three times to give up";

    // Enemy Data
    this._enemyPlayerName = enemyPlayerName;
    this._enemyLevel = enemyPlayerLevel;
    this._enemyHealth = enemyPlayerMaxHealth;
    this._enemyElement.src = Images.character.base.src;

    // Player Data
    this._playerHealthPriorToBattle = this._playerCharacter.stats.currentHealth;
    this._playerManaPriorToBattle = this._playerCharacter.stats.currentMana;
    this._playerCharacter.stats.currentHealth = this._playerCharacter.stats.maxHealth;
    this._playerCharacter.stats.currentMana = this._playerCharacter.stats.maxMana;
    this._playerIsCoinFlipper = playerIsCoinFlipper;
    this._playerElement.src = Images.character.base.src;

    // Interface
    this.openBattleScreen();
    this.loadPlayerSkills();
    this.printText(`You started a duel with ${this._enemyPlayerName}!`, () => {
      this.openActionBar();
    });

    // Firebase
    this._unsubscribe = FirebaseService.instance.addOnValueChangedListener(
      `${Key.dueling}/${enemyPlayerName}/${Key.turnData}`,
      (snapshot) => {
        if (!snapshot.val()) return;
        this._receivedEnemyData = true;
        this._enemyTurnData = snapshot.val();
      },
    );

    const unsubscribe = FirebaseService.instance.addOnChildRemovedListener(`${Key.dueling}`, (snapshot) => {
      if (snapshot.key == enemyPlayerName) {
        unsubscribe();
        setTimeout(() => {
          if (this.currentlyBattling) {
            this.currentlyBattling = false;
            this.closeActionBar();
            this.closeSkillBar();
            this.closeStanceBar();
            this.printText(`${enemyPlayerName} left..`, () => {
              setTimeout(() => {
                this.endBattle(true);
              }, 2000);
            });
          }
        }, 2000);
      }
    });
  }

  endBattle(unblock: boolean) {
    // Interface
    this.closeBattleScreen();
    this.blockButtons(this._actionButtons);
    setTimeout(() => {
      if (unblock) this._playerCharacter.unblockMovement();
      this.resetElement(this._playerElement);
      this.resetElement(this._enemyElement);
      this.currentlyBattling = false;
    }, 1000);

    // Player Data
    this._playerCharacter.stats.currentHealth = this._playerHealthPriorToBattle;
    this._playerCharacter.stats.currentMana = this._playerManaPriorToBattle;

    // Firebase
    FirebaseService.instance.cleanUpDuel();
    this._unsubscribe();
  }
}

export type TurnData = {
  selectedAbility: AbilitiyType;
  abilityDamage: number;
  selectedStance: Stance;
  coinFlipWinner: boolean;
};
