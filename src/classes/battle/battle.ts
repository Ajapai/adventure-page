import { AbilitiyType, Ability } from "../ability";
import { Images } from "../../helpers/image-helper";
import { PlayerCharacter } from "../character";
import { RandomHelper } from "../../helpers/random-helper";

export abstract class Battle {
  // Sections
  protected _battleScreen = document.getElementById("battleScreen");
  protected _battleTextBar = document.getElementById("textField");
  protected _actionSelectionBar = document.getElementById("actionSelection");
  protected _skillSelectionBar = document.getElementById("skillSelection");
  protected _stanceSelectionBar = document.getElementById("stanceSelection");

  // Display
  protected _playerElement = document.getElementById("playerSprite") as HTMLImageElement;
  protected _enemyElement = document.getElementById("enemySprite") as HTMLImageElement;
  protected _animationOnPlayerImage = document.getElementById("animationOnPlayer") as HTMLImageElement;
  protected _animationOnEnemyImage = document.getElementById("animationOnEnemy") as HTMLImageElement;
  protected _battleFieldElement = document.getElementById("battleBar");
  protected _gameOverScreen = document.createElement("img");

  // Buttons
  protected _restButton = document.getElementById("restButton") as HTMLInputElement;
  protected _skillButton = document.getElementById("skillButton") as HTMLInputElement;
  protected _attackButton = document.getElementById("attackButton") as HTMLInputElement;
  protected _fleeButton = document.getElementById("fleeButton") as HTMLInputElement;

  protected _skillButton1 = document.getElementById("skillButton1") as HTMLInputElement;
  protected _skillButton2 = document.getElementById("skillButton2") as HTMLInputElement;
  protected _skillButton3 = document.getElementById("skillButton3") as HTMLInputElement;
  protected _skillActionButton = document.getElementById("skillActionButton") as HTMLInputElement;

  protected _speedButton = document.getElementById("speedButton") as HTMLInputElement;
  protected _powerButton = document.getElementById("powerButton") as HTMLInputElement;
  protected _techniqueButton = document.getElementById("techniqueButton") as HTMLInputElement;

  protected _actionButtons = [this._skillButton, this._attackButton, this._restButton, this._fleeButton];
  protected _skillButtons = [this._skillButton1, this._skillButton2, this._skillButton3, this._skillActionButton];
  protected _stanceButtons = [this._speedButton, this._powerButton, this._techniqueButton];

  // Player fields
  protected _playerCharacter = PlayerCharacter.instance;
  protected _selectedAbility: Ability;
  protected _selectedStance: Stance;
  protected _equippedSkills: Ability[] = [];

  // Winner
  protected _stanceOutcome: StanceOutcome;
  protected _playerGoesFirst: boolean;

  // Abstract functions
  abstract flee(): void;
  abstract evaluateEnemyActions(): void;

  // Battle state
  currentlyBattling = false;

  protected constructor() {
    this._gameOverScreen.id = "gameOverScreen";
    this._gameOverScreen.src = Images.gameOver.src;

    this._skillButton.title = "Use one of your equipped skills";
    this._restButton.title = "Restores 10% of your max Health and 5% of your max Mana";

    this._speedButton.title = "Speed beats Power!";
    this._powerButton.title = "Power beats Technique!";
    this._techniqueButton.title = "Technique beats Speed!";
  }

  newBattle() {
    // Action Button events
    this._skillButton.onclick = this.skill.bind(this);
    this._attackButton.onclick = this.attack.bind(this);
    this._attackButton.title = Ability.create(AbilitiyType.Attack, this._playerCharacter.stats.level).description;
    this._restButton.onclick = this.rest.bind(this);

    // Skill Button Events
    this._skillButton1.onclick = () => {
      this.setChoosenSkill(0);
    };
    this._skillButton2.onclick = () => {
      this.setChoosenSkill(1);
    };
    this._skillButton3.onclick = () => {
      this.setChoosenSkill(2);
    };
    this._skillActionButton.onclick = () => {
      this.closeSkillBar();
      this.printText("Select an action.", this.openActionBar.bind(this));
    };

    // Stance Button Events
    this._speedButton.onclick = () => {
      this.applyStance(Images.character.speed.src, Stance.Speed);
    };
    this._powerButton.onclick = () => {
      this.applyStance(Images.character.power.src, Stance.Power);
    };
    this._techniqueButton.onclick = () => {
      this.applyStance(Images.character.technique.src, Stance.Technique);
    };
  }

  // Actions
  skill() {
    this.closeActionBar();
    this.printText("Select a skill.", this.openSkillBar.bind(this));
  }

  attack() {
    this.closeActionBar();
    this._selectedAbility = Ability.create(AbilitiyType.Attack, this._playerCharacter.stats.level);
    this.stanceSelection();
  }

  rest() {
    this.closeActionBar();
    this._selectedAbility = null;
    this.stanceSelection();
  }

  // Skills
  loadPlayerSkills() {
    for (const [key, value] of Object.entries(this._playerCharacter.stats.abilitySlots)) {
      const index = parseInt(key) - 1;
      this._equippedSkills[index] = Ability.create(value, this._playerCharacter.stats.level);
      if (this._equippedSkills[index]) this._skillButtons[index].title = this._equippedSkills[index].tooltip;
      this._skillButtons[index].value = this._equippedSkills[index]?.name ?? value;
    }
  }

  setChoosenSkill(index: 0 | 1 | 2) {
    this.closeSkillBar();
    if (this._equippedSkills[index].manaCost > this._playerCharacter.stats.currentMana) {
      this.printText("Not enaught mana..", this.openSkillBar.bind(this));
    } else {
      this._selectedAbility = this._equippedSkills[index];
      this.stanceSelection();
    }
  }

  // Stance
  stanceSelection() {
    this.printText("Select a stance.", () => {
      this.unblockButtons(this._stanceButtons);
      this.openStanceBar();
    });
  }

  applyStance(stanceImg: string, stance: Stance) {
    this.closeStanceBar();
    this._playerElement.src = stanceImg;
    this._selectedStance = stance;
    this.evaluateEnemyActions();
  }

  wonStanceBattle(enemyStance: Stance, inCaseOfDraw: () => boolean) {
    if (this._selectedStance == enemyStance) {
      this._stanceOutcome = StanceOutcome.Draw;
      return inCaseOfDraw();
    }
    switch (this._selectedStance) {
      case Stance.Speed: {
        this._stanceOutcome = enemyStance == Stance.Power ? StanceOutcome.Winner : StanceOutcome.Loser;
        break;
      }
      case Stance.Power: {
        this._stanceOutcome = enemyStance == Stance.Technique ? StanceOutcome.Winner : StanceOutcome.Loser;
        break;
      }
      case Stance.Technique: {
        this._stanceOutcome = enemyStance == Stance.Speed ? StanceOutcome.Winner : StanceOutcome.Loser;
        break;
      }
    }
    return this._stanceOutcome == StanceOutcome.Winner;
  }

  applyStanceDamage(damage: number, isPlayerDamage: boolean) {
    switch (this._stanceOutcome) {
      case StanceOutcome.Winner: {
        return isPlayerDamage ? Math.floor(damage * 1.5) : Math.ceil(damage * 0.5);
      }
      case StanceOutcome.Draw: {
        return damage;
      }
      case StanceOutcome.Loser: {
        return isPlayerDamage ? Math.ceil(damage * 0.5) : Math.floor(damage * 1.5);
      }
    }
  }

  // HTML manipulation
  printText(text: string, callback: () => void) {
    this._battleTextBar.style.opacity = "0";
    setTimeout(() => {
      this._battleTextBar.style.opacity = "1";
      this._battleTextBar.innerHTML = text;
      callback();
    }, 300);
  }

  openBattleScreen() {
    this._battleTextBar.style.opacity = "1";
    this._battleScreen.style.borderWidth = "1px";
    this._battleScreen.style.height = "56%";
  }
  closeBattleScreen() {
    this._battleTextBar.style.opacity = "0";
    this._battleScreen.style.borderWidth = "0";
    this._battleScreen.style.height = "0";
  }

  openActionBar() {
    if (!this.currentlyBattling) return;
    this.unblockButtons(this._actionButtons);
    this._actionSelectionBar.style.opacity = "1";
    this._actionSelectionBar.style.zIndex = "1";
  }
  closeActionBar() {
    this._actionSelectionBar.style.zIndex = "0";
    this._actionSelectionBar.style.opacity = "0";
    this.blockButtons(this._actionButtons);
  }

  openSkillBar() {
    if (!this.currentlyBattling) return;
    this.unblockButtons(this._skillButtons);
    this._skillSelectionBar.style.opacity = "1";
    this._skillSelectionBar.style.zIndex = "1";
  }
  closeSkillBar() {
    this._skillSelectionBar.style.zIndex = "0";
    this._skillSelectionBar.style.opacity = "0";
    this.blockButtons(this._skillButtons);
  }

  openStanceBar() {
    if (!this.currentlyBattling) return;
    this.unblockButtons(this._stanceButtons);
    this._stanceSelectionBar.style.opacity = "1";
    this._stanceSelectionBar.style.zIndex = "1";
  }
  closeStanceBar() {
    this._stanceSelectionBar.style.zIndex = "0";
    this._stanceSelectionBar.style.opacity = "0";
    this.blockButtons(this._stanceButtons);
  }

  blockButtons(buttonArray: HTMLInputElement[]) {
    buttonArray.forEach((button) => {
      button.disabled = true;
    });
  }
  unblockButtons(buttonArray: HTMLInputElement[]) {
    buttonArray.forEach((button) => {
      if (button.value == AbilitiyType.Empty) return;
      button.disabled = false;
    });
  }

  set animationOnEnemy(value: string) {
    this._animationOnPlayerImage.src = Images.none.src;
    this._animationOnEnemyImage.src = value;
  }

  set animationOnPlayer(value: string) {
    this._animationOnEnemyImage.src = Images.none.src;
    this._animationOnPlayerImage.src = value;
  }

  displayDamageNumber(value: number, isOnPlayer: boolean, isMana: boolean, damageDelay: number) {
    setTimeout(() => {
      const color = isMana ? "lightblue" : value > 0 ? "darkred" : "limegreen";
      const element = this.createNumberElement(color, isOnPlayer, isMana);
      element.innerHTML = Math.abs(value).toString();
      this._battleFieldElement.appendChild(element);
      setTimeout(() => {
        element.style.top = "0";
        element.style.opacity = "0";
        setTimeout(() => {
          element.remove();
        }, 2000);
      }, 50);
    }, damageDelay);
  }

  createNumberElement(color: string, isOnPlayer: boolean, isMana: boolean) {
    const element = document.createElement("div");
    element.style.transition = "2s linear";
    element.style.position = "absolute";
    element.style.fontWeight = "bold";
    element.style.fontSize = "150%";
    element.style.color = color;
    element.style.top = "25%";
    if (isOnPlayer) element.style.right = `${isMana ? 25 : RandomHelper.randomRange(15, 20)}%`;
    else element.style.left = `${RandomHelper.randomRange(15, 20)}%`;
    return element;
  }

  deathAnimation(element: HTMLImageElement) {
    element.style.transition = "2s linear";
    element.style.opacity = "0";
  }

  resetElement(element: HTMLImageElement) {
    element.style.transition = "0s";
    element.style.opacity = "1";
  }
}

export enum Stance {
  Speed = "Speed",
  Power = "Power",
  Technique = "Technique",
}

export enum StanceOutcome {
  Winner = "Winner",
  Draw = "Draw",
  Loser = "Loser",
}
