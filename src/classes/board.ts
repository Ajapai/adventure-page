import { Terrain, TerrainType } from "./terrain";
import { FirebaseService } from "../services/firebase-service";
import { Tile } from "./tile";

export class Board {
  private static _instance: Board;
  private readonly _firebaseService: FirebaseService;
  private readonly _tiles: Tile[][] = [];
  private readonly _element: HTMLElement = document.getElementById("board");
  private readonly _screenChagedEvent: Event = new Event("screenChanged");

  private buildBoard(): void {
    for (let x = 0; x < 16; x++) {
      this._tiles[x] = [];
      for (let y = 0; y < 20; y++) {
        this._tiles[x][y] = new Tile(document.createElement("img"));
        this._tiles[x][y].element.className = "tile";
        this._tiles[x][y].element.id = x + "-" + y;
        this._element.appendChild(this._tiles[x][y].element);
      }
    }
  }

  async loadScreen(): Promise<void> {
    return this._firebaseService.getScreen(this.boardPosX, this.boardPosY).then((matrix) => {
      this.fromStringMatrix(matrix.val());
      this._element.dispatchEvent(this._screenChagedEvent);
    });
  }

  private constructor(boardPosX: number, boardPosY: number) {
    this._firebaseService = FirebaseService.instance;
    this.boardPosX = boardPosX;
    this.boardPosY = boardPosY;

    this.buildBoard();
    this.loadScreen();
  }

  static init(boardPosX: number, boardPosY: number) {
    if (this._instance) return this._instance;
    this._instance = new Board(boardPosX, boardPosY);
    return this._instance;
  }

  static get instance() {
    return this._instance ?? this.init(9, 9);
  }

  boardPosX: number;
  boardPosY: number;

  get tiles() {
    return this._tiles;
  }

  addCharacterElement(characterElement: HTMLImageElement) {
    this._element.appendChild(characterElement);
  }

  screenTransition(offsetX: number, offsetY: number): Promise<void> {
    this.boardPosX += offsetX;
    this.boardPosY += offsetY;
    return this.loadScreen();
  }

  fromStringMatrix(matrix: string[][]) {
    for (let x = 0; x < 16; x++) {
      for (let y = 0; y < 20; y++) {
        if (matrix && matrix[x] && matrix[x][y]) {
          this._tiles[x][y].terrain = Terrain.create(matrix[x][y] as TerrainType);
        } else {
          this._tiles[x][y].terrain = Terrain.create(TerrainType.Mountain);
        }
      }
    }
  }
}
