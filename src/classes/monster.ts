import { AbilitiyType, Ability } from "./ability";
import { Images, StanceImageCollection } from "../helpers/image-helper";
import { RandomHelper } from "../helpers/random-helper";
import { Stance } from "./battle/battle";

export abstract class Monster {
  abstract readonly name: string;
  abstract readonly battleText: string;
  abstract readonly stanceTendency: StanceTendency;
  protected readonly _level: number;
  protected abstract _abilities: Ability[];
  abstract get maxHealth(): number;
  abstract get expValue(): number;
  readonly imageCollection: StanceImageCollection;

  constructor(imageCollection: StanceImageCollection, level: number) {
    this.imageCollection = imageCollection;
    this._level = level;
  }

  get tendencyTotal() {
    return (
      this.stanceTendency.speedTendency + this.stanceTendency.powerTendency + this.stanceTendency.techniqueTendency
    );
  }

  generateStance(): Stance {
    const generatedNumber = RandomHelper.randomRange(1, this.tendencyTotal);
    if (generatedNumber <= this.stanceTendency.speedTendency) {
      return Stance.Speed;
    } else if (generatedNumber <= this.stanceTendency.speedTendency + this.stanceTendency.powerTendency) {
      return Stance.Power;
    } else {
      return Stance.Technique;
    }
  }

  getStanceImage(stance: Stance) {
    switch (stance) {
      case Stance.Speed: {
        return this.imageCollection.speed.src;
      }
      case Stance.Power: {
        return this.imageCollection.power.src;
      }
      case Stance.Technique: {
        return this.imageCollection.technique.src;
      }
    }
  }

  chooseAbility() {
    return this._abilities[RandomHelper.randomRange(0, this._abilities.length - 1)];
  }
}

export class Blob extends Monster {
  readonly name = "Blob";
  readonly battleText: string;
  readonly stanceTendency = new StanceTendency(8, 2, 4);
  readonly _abilities: Ability[] = [];

  get maxHealth(): number {
    return 14 + this._level * this._level;
  }
  get expValue(): number {
    return 10 + 5 * this._level;
  }

  constructor(level: number) {
    super(Images.blob, level);
    this._abilities.push(Ability.create(AbilitiyType.Attack, level));
    this.battleText = `A small blob (lv.${level}) ran into you`;
  }
}

export class Flower extends Monster {
  readonly name = "Flower";
  readonly battleText: string;
  readonly stanceTendency = new StanceTendency(3, 1, 5);
  readonly _abilities: Ability[] = [];
  get maxHealth(): number {
    return 10 + Math.round(this._level * this._level * 1.2);
  }
  get expValue(): number {
    return 10 + Math.ceil(this._level * 6.5);
  }

  constructor(level: number) {
    super(Images.flower, level);
    this._abilities.push(Ability.create(AbilitiyType.Attack, level));
    this._abilities.push(Ability.create(AbilitiyType.LeafCutter, level));
    this.battleText = `You stepped on some sort of flower (lv.${level})`;
  }
}

export class Treant extends Monster {
  name = "Treant";
  battleText: string;
  readonly stanceTendency = new StanceTendency(3, 9, 6);
  protected _abilities: Ability[] = [];

  get maxHealth(): number {
    return 10 + this._level * 10;
  }
  get expValue(): number {
    return 15 + this._level * 8;
  }

  constructor(level: number) {
    super(Images.treant, level);
    this._abilities.push(Ability.create(AbilitiyType.Attack, level));
    this._abilities.push(Ability.create(AbilitiyType.RockThrow, level));
    this.battleText = `A little treant (lv.${level}) blocks your path`;
  }
}

class StanceTendency {
  readonly speedTendency: number;
  readonly powerTendency: number;
  readonly techniqueTendency: number;

  constructor(speed: number, power: number, technique: number) {
    this.speedTendency = speed;
    this.powerTendency = power;
    this.techniqueTendency = technique;
  }
}
