import { AbilitiyType } from "./ability";
import { FirebaseService } from "../services/firebase-service";
import { Images } from "../helpers/image-helper";
import { SkillHelper } from "../helpers/skill-helper";

export class CharacterStats {
  private static _instance: CharacterStats;
  private readonly _firebaseService: FirebaseService;
  private readonly _levelElement = document.getElementById("characterLevel");
  private readonly _hitPointsElement = document.getElementById("hitPoints");
  private readonly _healthBarElement = document.getElementById("healthBar");
  private readonly _manaPointsElement = document.getElementById("manaPoints");
  private readonly _manaBarElement = document.getElementById("manaBar");
  private readonly _expBarElement = document.getElementById("expBar");
  private readonly _skillPointsElement = document.getElementById("skillPoints");
  private _level: number;
  private _skillPoints: number;
  private _currentHealth: number;
  private _currentMana: number;
  private _expiriencePoints: number;
  private _abilitySlots: AbilitySlots;
  private _skillTree: SkillTree;

  private constructor() {
    this._firebaseService = FirebaseService.instance;
    this.loadCharacterStats();
  }

  static get instance() {
    if (!this._instance) {
      this._instance = new CharacterStats();
    }
    return this._instance;
  }

  async loadCharacterStats() {
    return this._firebaseService.getCurrentPlayerStats().then((snapshot) => {
      this.setCharacterStats(snapshot.val() ?? {});
    });
  }

  get level() {
    return this._level;
  }
  set level(value) {
    this._level = value;
    this._levelElement.innerHTML = "Lv. " + this._level;
  }

  get expForLvlUp() {
    return this._level * (this._level + 1) * 50;
  }

  get currentExp() {
    return this._expiriencePoints;
  }
  set currentExp(value) {
    this._expiriencePoints = value;
    this._expBarElement.style.width = (100 * this._expiriencePoints) / this.expForLvlUp + "%";
  }

  get maxHealth() {
    return 50 + this._level * (2 * this._level - 2);
  }

  get currentHealth() {
    return this._currentHealth;
  }
  set currentHealth(value) {
    this._currentHealth = value > this.maxHealth ? this.maxHealth : value < 0 ? 0 : value;
    this._hitPointsElement.innerHTML = `${this._currentHealth}/${this.maxHealth}`;
    this._healthBarElement.style.width = (100 * this._currentHealth) / this.maxHealth + "%";
  }

  get maxMana() {
    return 10 + this._level * (this._level - 1);
  }

  get currentMana() {
    return this._currentMana;
  }
  set currentMana(value) {
    this._currentMana = value > this.maxMana ? this.maxMana : value < 0 ? 0 : value;
    this._manaPointsElement.innerHTML = `${this._currentMana}/${this.maxMana}`;
    this._manaBarElement.style.width = (100 * this._currentMana) / this.maxMana + "%";
  }

  get skillPoints() {
    return this._skillPoints;
  }
  set skillPoints(value: number) {
    this._skillPoints = value;
    this._skillPointsElement.innerHTML = `Skill Points: ${this._skillPoints}`;
  }

  get skillTree() {
    return this._skillTree;
  }

  get abilitySlots() {
    return this._abilitySlots;
  }

  get hasSpikyBoots() {
    return this._skillTree.spikyBoots.unlocked;
  }

  get hasFloatingBoots() {
    return this._skillTree.floatingBoots.unlocked;
  }

  addExp(amount: number) {
    if (this._expiriencePoints + amount >= this.expForLvlUp) {
      this._expiriencePoints += amount;
      this.lvlUp();
    } else {
      this.currentExp += amount;
    }
  }

  lvlUp() {
    const expForLvlUp = this.expForLvlUp;
    this.level += 1;
    this._skillPoints += 1;
    this.currentExp -= expForLvlUp;
    this.currentHealth = this.maxHealth;
    this.currentMana = this.maxMana;
  }

  unlockSkill(skill: Skill) {
    if (this.skillPoints <= 0) return;
    this.skillPoints--;
    skill.unlocked = true;
    this.saveCharacterStats();
  }

  equipAbility(slot: number, ability: AbilitiyType) {
    this._abilitySlots[slot as 1 | 2 | 3] = ability;
    this.saveCharacterStats();
  }

  setCharacterStats(stats: statsSaveObject) {
    this.level = stats.level ?? 1;
    this.currentHealth = stats.currentHealth ?? 50;
    this.currentMana = stats.currentMana ?? 10;
    this.currentExp = stats.expiriencePoints ?? 0;
    this.skillPoints = stats.skillPoints ?? 1;
    this._skillTree = stats.skillTree ? SkillTree.fromSaveObject(stats.skillTree) : new SkillTree();
    this._abilitySlots = stats.abilitySlots ?? {
      1: AbilitiyType.Empty,
      2: AbilitiyType.Empty,
      3: AbilitiyType.Empty,
    };
    SkillHelper.init();
  }

  saveCharacterStats() {
    const stats: statsSaveObject = {
      level: this._level,
      currentHealth: this._currentHealth,
      currentMana: this._currentMana,
      expiriencePoints: this._expiriencePoints,
      skillPoints: this._skillPoints,
      skillTree: this._skillTree.createSaveObject(),
      abilitySlots: this._abilitySlots,
    };
    this._firebaseService.setCurrentPlayerStats(stats);
  }
}

export class SkillTree {
  spikyBoots: Skill = {
    name: "Spiky Boots",
    description: "'Spiky Boots' enable you to hike over rocky terrain!",
    unlocked: false,
    isAbility: false,
    image: Images.skill.spikyBoots.src,
  };
  floatingBoots: Skill = {
    name: "Floating Boots",
    description: "'Floating Boots' make it possible to walk on water!",
    unlocked: false,
    isAbility: false,
    image: Images.skill.floatingBoots.src,
  };
  rockThrow: Skill = {
    name: "Rock Throw",
    description: "'Rock Throw' allows you to throw a rock at your enemy!\n High damage variance for a low mana cost.",
    unlocked: false,
    isAbility: AbilitiyType.RockThrow,
    image: Images.skill.rockThrow.src,
  };
  flame: Skill = {
    name: "Flame",
    description: "'Flame' lets you cast a flame onto your enemy!\n Reliable damage for a medium mana cost.",
    unlocked: false,
    isAbility: AbilitiyType.Flame,
    image: Images.skill.flame.src,
  };
  heal: Skill = {
    name: "Heal",
    description: "'Heal' can restore a lot of health,\n but comes at a high mana cost!",
    unlocked: false,
    isAbility: AbilitiyType.Heal,
    image: Images.skill.heal.src,
  };

  createSaveObject() {
    const obj: { [name: string]: boolean } = {};
    for (const [key, value] of Object.entries(this)) {
      obj[key] = value.unlocked;
    }
    return obj;
  }

  static fromSaveObject(obj: { [name: string]: boolean }) {
    const skillTree = new SkillTree();
    for (const [key] of Object.entries(skillTree)) {
      (skillTree as unknown as { [name: string]: Skill })[key].unlocked = obj[key] ?? false;
    }
    return skillTree;
  }
}

export type Skill = {
  name: string;
  description: string;
  unlocked: boolean;
  isAbility: false | AbilitiyType;
  image: string;
};

type AbilitySlots = {
  1: AbilitiyType;
  2: AbilitiyType;
  3: AbilitiyType;
};

type statsSaveObject = {
  level: number;
  currentHealth: number;
  currentMana: number;
  expiriencePoints: number;
  skillPoints: number;
  skillTree: { [name: string]: boolean };
  abilitySlots: AbilitySlots;
};
