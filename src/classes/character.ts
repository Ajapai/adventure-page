import { FirebaseService, Key } from "../services/firebase-service";
import { Board } from "./board";
import { CharacterStats } from "./character-stats";
import { ChatService } from "../services/chat-service";
import { Images } from "../helpers/image-helper";
import { MonsterBattle } from "./battle/monster-battle";
import { PlayerBattle } from "./battle/player-battle";
import { RandomHelper } from "../helpers/random-helper";
import { StanceOutcome } from "./battle/battle";
import { Traversable } from "./terrain";

// Directional constants
const UP = -1;
const DOWN = 1;
const LEFT = -1;
const RIGHT = 1;
const NONE = 0;

export abstract class Character {
  protected readonly _firebaseService: FirebaseService;
  protected readonly _board: Board;
  protected readonly _element: HTMLImageElement;
  protected _charPosX: number;
  protected _charPosY: number;

  constructor(charPosX: number, charPosY: number) {
    this._firebaseService = FirebaseService.instance;
    this._board = Board.instance;
    this._element = document.createElement("img");
    this._element.src = Images.character.base.src;
    this._charPosX = charPosX;
    this._charPosY = charPosY;
  }

  addElementToBoard() {
    this._board.addCharacterElement(this._element);
  }

  removeElementFromBoard() {
    this._element.remove();
  }
}

// Player
export class PlayerCharacter extends Character {
  private static _instance: PlayerCharacter;
  private readonly _characterStats = CharacterStats.instance;
  private _moveBlocked = false;
  private _steps = 0;
  private _flagUp = false;

  private constructor(charPosX: number, charPosY: number) {
    super(charPosX, charPosY);
    document.getElementById("characterName").innerHTML = this._firebaseService.currentUserName;
    this._element.id = "playerCharacter";
    this.addElementToBoard();
    this.reloadElementPosition();
  }

  static init(charPosX: number, charPosY: number) {
    if (this._instance) return this._instance;
    this._instance = new PlayerCharacter(charPosX, charPosY);
    return this._instance;
  }

  static get instance() {
    return this._instance ?? this.init(9, 9);
  }

  get stats() {
    return this._characterStats;
  }

  get currentlyBattling() {
    return MonsterBattle.instance.currentlyBattling || PlayerBattle.instance.currentlyBattling;
  }

  get currentlyFlagging() {
    return this._flagUp;
  }

  rest(stanceOutcome: StanceOutcome): { health: number; mana: number } {
    let health = this._characterStats.maxHealth / 20;
    let mana = this._characterStats.maxMana / 10;
    switch (stanceOutcome) {
      case StanceOutcome.Winner: {
        health = Math.round(health * 1.5);
        mana = Math.round(mana * 1.5);
        break;
      }
      case StanceOutcome.Draw: {
        health = Math.round(health * 1.5);
        mana = Math.round(mana * 1.5);
        break;
      }
      case StanceOutcome.Loser: {
        health = Math.round(health * 0.5);
        mana = Math.round(mana * 0.5);
      }
    }
    this._characterStats.currentHealth += health;
    this._characterStats.currentMana += mana;
    return { health: health, mana: mana };
  }

  // PvP
  toggleFlag() {
    if (this.currentlyBattling) return;
    if (!this._flagUp) {
      this._moveBlocked = true;
      this.flagUp();
    } else {
      this._moveBlocked = false;
      this.removeFlag();
    }
  }

  private flagUp() {
    this._flagUp = true;
    this._element.src = Images.character.duel.src;
    this._firebaseService.setFlagForPvP(true).then(() => {
      const unsubscribe = this._firebaseService.addOnValueChangedListener(
        `${Key.flaggingPlayers}/${this._firebaseService.currentUserName}`,
        (snapshot) => {
          if (snapshot.val()) {
            unsubscribe();
            this.startDuel(snapshot.val());
          }
        },
      );
    });
  }

  startDuel(enemyPlayerName: string) {
    this._firebaseService.setUpDuel(enemyPlayerName, this.stats.level, this.stats.maxHealth);
    const unsubscribe = this._firebaseService.addOnValueChangedListener(
      `${Key.dueling}/${enemyPlayerName}`,
      (snapshot) => {
        if (snapshot.val() && snapshot.val()[Key.enemyPlayer] == this._firebaseService.currentUserName) {
          unsubscribe();
          this.removeFlag();
          PlayerBattle.instance.innitiateBattle(
            enemyPlayerName,
            snapshot.val()[Key.level],
            snapshot.val()[Key.maxHealth],
            true,
          );
        } else if (snapshot.val() && snapshot.val()[Key.enemyPlayer] != this._firebaseService.currentUserName) {
          ChatService.instance.printMessage(`System: ${enemyPlayerName} is already dueling someone else..`);
        }
      },
    );
  }

  awaitDuel(enemyPlayerName: string) {
    const unsubscribe = this._firebaseService.addOnValueChangedListener(
      `${Key.dueling}/${enemyPlayerName}`,
      (snapshot) => {
        if (snapshot.val() && snapshot.val()[Key.enemyPlayer] == this._firebaseService.currentUserName) {
          unsubscribe();
          this._firebaseService.setUpDuel(enemyPlayerName, this.stats.level, this.stats.maxHealth);
          PlayerBattle.instance.innitiateBattle(
            enemyPlayerName,
            snapshot.val()[Key.level],
            snapshot.val()[Key.maxHealth],
            false,
          );
        }
      },
    );
  }

  private removeFlag() {
    this._flagUp = false;
    this._element.src = Images.character.base.src;
    this._firebaseService.setFlagForPvP(false);
  }

  // Movement
  private canMove(traversable: Traversable) {
    switch (traversable) {
      case Traversable.Always: {
        return true;
      }
      case Traversable.SpikyBoots: {
        return this._characterStats.hasSpikyBoots;
      }
      case Traversable.FloatingBoots: {
        return this._characterStats.hasFloatingBoots;
      }
      case Traversable.Never: {
        return false;
      }
    }
  }

  private reloadElementPosition() {
    this._element.style.top = (this._charPosX * 6.25).toString() + "%";
    this._element.style.left = (this._charPosY * 5).toString() + "%";
    this._firebaseService.setCharacterPosition(
      this._board.boardPosX,
      this._board.boardPosY,
      this._charPosX,
      this._charPosY,
    );
  }

  private moveTransition(offsetX: number, offsetY: number) {
    this._moveBlocked = true;
    setTimeout(() => {
      this._board.screenTransition(offsetX, offsetY).then(() => {
        if (offsetX) this._charPosX = offsetX < 0 ? 16 : -1;
        if (offsetY) this._charPosY = offsetY < 0 ? 20 : -1;
        this.removeElementFromBoard();
        this.reloadElementPosition();
        this.addElementToBoard();
        setTimeout(this.moveIn.bind(this), 50);
      });
    }, 300);
  }

  private moveIn() {
    if (this._charPosX == -1) this._charPosX++;
    if (this._charPosX == 16) this._charPosX--;
    if (this._charPosY == -1) this._charPosY++;
    if (this._charPosY == 20) this._charPosY--;
    this.reloadElementPosition();

    setTimeout(() => {
      this._moveBlocked = false;
    }, 200);
  }

  private moveEvaluate() {
    this._steps += 1;
    if (this._steps >= 10) {
      this.stats.currentHealth += 2;
      this.stats.currentMana += 1;
      this.stats.saveCharacterStats();
      this._steps -= 15;
    }
    this.reloadElementPosition();
    const currentTerrain = this._board.tiles[this._charPosX][this._charPosY].terrain;
    if (RandomHelper.randomRange(0, 100) <= currentTerrain.encounterRate) {
      this._moveBlocked = true;
      MonsterBattle.instance.innitiateBattle(
        currentTerrain.encounters[RandomHelper.randomRange(0, currentTerrain.encounters.length - 1)],
      );
    }
  }

  unblockMovement() {
    this._moveBlocked = false;
  }

  moveUp() {
    if (this._moveBlocked) return;
    if (
      this._charPosX === 0 ||
      (this._board.tiles[this._charPosX - 1][this._charPosY] &&
        this.canMove(this._board.tiles[this._charPosX - 1][this._charPosY].terrain.traversable))
    ) {
      this._charPosX--;

      if (this._charPosX < 0) {
        this.reloadElementPosition();
        this.moveTransition(UP, NONE);
      } else {
        this.moveEvaluate();
      }
    }
  }

  moveDown() {
    if (this._moveBlocked) return;
    if (
      this._charPosX === 15 ||
      (this._board.tiles[this._charPosX + 1][this._charPosY] &&
        this.canMove(this._board.tiles[this._charPosX + 1][this._charPosY].terrain.traversable))
    ) {
      this._charPosX++;

      if (this._charPosX > 15) {
        this.reloadElementPosition();
        this.moveTransition(DOWN, NONE);
      } else {
        this.moveEvaluate();
      }
    }
  }

  moveLeft() {
    if (this._moveBlocked) return;
    if (
      this._charPosY === 0 ||
      (this._board.tiles[this._charPosX][this._charPosY - 1] &&
        this.canMove(this._board.tiles[this._charPosX][this._charPosY - 1].terrain.traversable))
    ) {
      this._charPosY--;

      if (this._charPosY < 0) {
        this.reloadElementPosition();
        this.moveTransition(NONE, LEFT);
      } else {
        this.moveEvaluate();
      }
    }
  }

  moveRight() {
    if (this._moveBlocked) return;
    if (
      this._charPosY === 19 ||
      (this._board.tiles[this._charPosX][this._charPosY + 1] &&
        this.canMove(this._board.tiles[this._charPosX][this._charPosY + 1].terrain.traversable))
    ) {
      this._charPosY++;

      if (this._charPosY > 19) {
        this.reloadElementPosition();
        this.moveTransition(NONE, RIGHT);
      } else {
        this.moveEvaluate();
      }
    }
  }

  // Reset
  resetOnDeath() {
    this.removeElementFromBoard();
    this._charPosX = 9;
    this._charPosY = 9;
    this._board.boardPosX = 9;
    this._board.boardPosY = 9;
    this.reloadElementPosition();
    this._board.loadScreen().then(this.addElementToBoard.bind(this));
    this.stats.currentHealth = this.stats.maxHealth;
    this.stats.currentMana = this.stats.maxMana;
    this.stats.currentExp = 0;
    this.stats.saveCharacterStats();
  }
}

// Other Player
export class OtherPlayerCharacter extends Character {
  private readonly _username: string;
  private _boardPosX: number;
  private _boardPosY: number;

  private setElementPosition() {
    this._element.style.top = (this._charPosX * 6.25).toString() + "%";
    this._element.style.left = (this._charPosY * 5).toString() + "%";
  }

  constructor(username: string, charPosX: number, charPosY: number, boardPosX: number, boardPosY: number) {
    super(charPosX, charPosY);
    this._username = this._element.id = username;
    this._element.classList.add("otherPlayerCharacter");
    this._boardPosX = boardPosX;
    this._boardPosY = boardPosY;
    this._charPosX = charPosX;
    this._charPosY = charPosY;
    if (this.onCurrentScreen) {
      this.addElementToBoard();
      this.setElementPosition();
    }

    this._firebaseService.addOnChildChangedListener(`${Key.players}/${this._username}/${Key.positions}`, (snapshot) => {
      if (snapshot.key === Key.charPosition) {
        this._charPosX = snapshot.val()[Key.charPosX];
        this._charPosY = snapshot.val()[Key.charPosY];
        if (this.onCurrentScreen) this.setElementPosition();
      } else if (snapshot.key === Key.boardPosition) {
        this._boardPosX = snapshot.val()[Key.boardPosX];
        this._boardPosY = snapshot.val()[Key.boardPosY];
        this.updateElementVisibility();
      }
    });
    document.getElementById("board").addEventListener("screenChanged", () => {
      this.updateElementVisibility();
    });
  }

  get onCurrentScreen(): boolean {
    return this._board.boardPosX === this._boardPosX && this._board.boardPosY === this._boardPosY;
  }

  get imageElement() {
    return this._element;
  }

  updateElementVisibility() {
    if (this.onCurrentScreen) {
      this.addElementToBoard();
      this.setElementPosition();
    } else {
      this.removeElementFromBoard();
    }
  }
}
