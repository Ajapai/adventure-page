import { Terrain } from "./terrain";

export class Tile {
  readonly element: HTMLImageElement;
  private _terrain: Terrain;

  constructor(htmlElement: HTMLImageElement) {
    this.element = htmlElement;
  }

  get terrain(): Terrain {
    return this._terrain;
  }

  set terrain(value: Terrain) {
    this._terrain = value;
    this.element.src = this._terrain.image.toString();
  }
}
