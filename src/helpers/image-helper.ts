export class StanceImageCollection {
  base = new Image();
  speed = new Image();
  power = new Image();
  technique = new Image();
}

export class Images {
  static readonly none = new Image();
  static readonly gameOver = new Image();

  // Player character
  static readonly character = {
    base: new Image(),
    speed: new Image(),
    power: new Image(),
    technique: new Image(),
    duel: new Image(),
  };

  // Monsters
  static readonly blob = new StanceImageCollection();
  static readonly flower = new StanceImageCollection();
  static readonly treant = new StanceImageCollection();

  // Abilities
  static readonly rest = new Image();
  static readonly attack = new Image();
  static readonly flame = new Image();
  static readonly heal = new Image();
  static readonly rockThrow = new Image();
  static readonly leafCutter = new Image();

  // Skills
  static readonly skill = {
    flame: new Image(),
    heal: new Image(),
    rockThrow: new Image(),
    spikyBoots: new Image(),
    floatingBoots: new Image(),
  };

  // Terrain
  static readonly desert = new Image();
  static readonly forest = new Image();
  static readonly grass = new Image();
  static readonly mountain = new Image();
  static readonly mud = new Image();
  static readonly water = new Image();

  static init() {
    this.none.src = new URL("../images/none.png", import.meta.url).toString();
    this.gameOver.src = new URL("../images/game-over.png", import.meta.url).toString();

    // Character
    this.character.base.src = new URL("../images/character.png", import.meta.url).toString();
    this.character.speed.src = new URL("../images/character-speed.png", import.meta.url).toString();
    this.character.power.src = new URL("../images/character-power.png", import.meta.url).toString();
    this.character.technique.src = new URL("../images/character-technique.png", import.meta.url).toString();
    this.character.duel.src = new URL("../images/character-duel.png", import.meta.url).toString();

    // Monster
    this.blob.base.src = new URL("../images/monster/blob.png", import.meta.url).toString();
    this.blob.speed.src = new URL("../images/monster/blob-speed.png", import.meta.url).toString();
    this.blob.power.src = new URL("../images/monster/blob-power.png", import.meta.url).toString();
    this.blob.technique.src = new URL("../images/monster/blob-technique.png", import.meta.url).toString();
    this.flower.base.src = new URL("../images/monster/flower.png", import.meta.url).toString();
    this.flower.speed.src = new URL("../images/monster/flower-speed.png", import.meta.url).toString();
    this.flower.power.src = new URL("../images/monster/flower-power.png", import.meta.url).toString();
    this.flower.technique.src = new URL("../images/monster/flower-technique.png", import.meta.url).toString();
    this.treant.base.src = new URL("../images/monster/treant.png", import.meta.url).toString();
    this.treant.speed.src = new URL("../images/monster/treant-speed.png", import.meta.url).toString();
    this.treant.power.src = new URL("../images/monster/treant-power.png", import.meta.url).toString();
    this.treant.technique.src = new URL("../images/monster/treant-technique.png", import.meta.url).toString();

    // Abilities
    this.rest.src = new URL("../images/abilities/rest.gif", import.meta.url).toString();
    this.attack.src = new URL("../images/abilities/attack.gif", import.meta.url).toString();
    this.flame.src = new URL("../images/abilities/flame.gif", import.meta.url).toString();
    this.heal.src = new URL("../images/abilities/heal.gif", import.meta.url).toString();
    this.rockThrow.src = new URL("../images/abilities/rock-throw.gif", import.meta.url).toString();
    this.leafCutter.src = new URL("../images/abilities/leaf-cutter.gif", import.meta.url).toString();

    // Skills
    this.skill.flame.src = new URL("../images/skills/flame.png", import.meta.url).toString();
    this.skill.heal.src = new URL("../images/skills/heal.png", import.meta.url).toString();
    this.skill.rockThrow.src = new URL("../images/skills/rock-throw.png", import.meta.url).toString();
    this.skill.spikyBoots.src = new URL("../images/skills/spiky-boots.png", import.meta.url).toString();
    this.skill.floatingBoots.src = new URL("../images/skills/floating-boots.png", import.meta.url).toString();

    // Terrain
    this.desert.src = new URL("../images/terrain/desert.png", import.meta.url).toString();
    this.forest.src = new URL("../images/terrain/forest.png", import.meta.url).toString();
    this.grass.src = new URL("../images/terrain/grass.png", import.meta.url).toString();
    this.mountain.src = new URL("../images/terrain/mountain.png", import.meta.url).toString();
    this.mud.src = new URL("../images/terrain/mud.png", import.meta.url).toString();
    this.water.src = new URL("../images/terrain/water.png", import.meta.url).toString();
  }
}
